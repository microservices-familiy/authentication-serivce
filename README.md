# Service Authentication on Microservices Family
Microservices adalah suatu konsep pengembangan sistem / aplikasi yang terbagi menjadi beberapa module yang akan dijadikan aplikasi - aplikasi kecil yang nantinya akan disebut dengan services, setiap service harus mempunyai procedure yang dapat menerima request client dengan komunikasi data yang tepat antar service disertai dengan keamanan yang mumpuni. umumnya dalam microservices famility terdapat 2 jenis service yaitu service authentication dan service transactional.

## Service Authentication
Pada service authentication ini, service harus bisa sharing data user yang sedang login dengan service transactional agar tercipta relasi data yang baik guna mendukung performa dari infrastruktur, namun juga tidak menghilangkan unsur keamanan. Berikut fitur yang terkandung pada service authentication ini :
- Multi Data Definition Language (DDL)
- Shared User Login
- Log Tracer
- One Key - One Form (OKOF) / CSRF (Cross-site Request Forgery)
- Object Server Storage

## Requirement Service
- Redis
- SQLite3
- S3 Minio
- MySQL
- Apache2
- Composer
- Valet runtime (optional)

## Install Service

```bash
git clone https://gitlab.com/microservices-familiy/authentication-serivce.git auth_service
cd auth_service
cp -r .env.example .env
composer install
php artisan key:generate
php artisan migrate --seed
```
Sesuaikan konfigurasi infrastruktur anda pada file **_.env_** jika sudah sekarang coba buka _Browser_ anda lalu ketika _auth_service.test_. Jika anda tidak menemukan kesalahan pada _CLI_ anda, maka anda telah berhasil install **Service Authentication**.

## Documentation Transactional Data
Ketika keseluruhan tahap diatas telah berhasil anda jalankan, selanjutnya adalah penjelasan terkait mekanisme **Transaksi Data**. Pertama check routing dengan cara
```bash
php artisan route:list
```
Maka akan muncul list table seperti gambar berikut
![alt text](https://raw.githubusercontent.com/bayuzsa/doc/main/Screen%20Shot%202021-02-26%20at%2011.03.09%20PM.png "List Route")
pada colom **Middleware**  jika terdapat tulisan **auth** artinya ketika akses **path** tersebut harus disertai header
```bash
Authorization: Beare [token]
```
Jika dalam colom **Middleware** terdapat tulisan **csrf** maka ketika akses **path** tersebut harus disertai header
```bash
X-Csrf-Token: Client [token]
```
Token **Authorization** didapat pada respon body ketika berhasil melakukan **Login** dan token **CSRF** didapat dan dilihat diheader respon atau setiap berhasil melakukan request dengan method POST maka pada header respon juga disematkan token baru atau juga bisa request token form pada path **token-form**. dengan kata lain jika terdapat **Middleware: csrf** artinya itu **_One Key - One Form_**

Selebihnya bisa langsung import file postman pada aplikasi postman.
terimakasih

### Credit
* Ban Serep
* Bataraguru
