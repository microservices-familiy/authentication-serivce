<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    public function index()
    {
        return response()->json(['code'=>200,'error'=>false,'data'=>Users::all()], 200);
    }

    public function store(Request $request)
    {
        $upload = Storage::put('', $request->file('photo'));

        $data = new Users();
        $data->nama_lengkap = $request->post('nama_lengkap');
        $data->email = $request->post('email');
        $data->no_telp = $request->post('no_telp');
        $data->username = $request->post('username');
        $data->password = Hash::make($request->post('password'));
        $data->pict_profile = basename($upload);
        $data->save();
        return response()->json(['code'=>201,'error'=>false,'data'=>$data], 201);
    }

    public function show($id)
    {
        return response()->json(['code'=>200,'error'=>false,'data'=>Users::find($id)]);
    }

    public function update(Request $request, $id)
    {
        $upload = Storage::put('',$request->file('photo'));

        $data = Users::find($id);
        Storage::delete($data->pict_profile);
        $data->nama_lengkap = $request->post('nama_lengkap');
        $data->email = $request->post('email');
        $data->no_telp = $request->post('no_telp');
        $data->username = $request->post('username');
        $data->password = Hash::make($request->post('password'));
        $data->pict_profile = basename($upload);
        $data->save();
        return response()->json(['code'=>200,'error'=>false,'data'=>Users::all()], 200);
    }

    public function destroy($id)
    {
        $us = Users::find($id);
        Storage::delete($us->pict_profile);
        Users::destroy($id);
        return response()->json(['code'=>200,'error'=>false,'msg'=>'Data telah dihapus'], 200);
    }
}
