<?php

namespace App\Http\Controllers;

use App\Repositories\PredisRepositori;
use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller{

    protected $conn, $hash;

    public function __construct(PredisRepositori $conn){
        $this->conn = $conn->predis();
    }

    public function login(Request $request){
        $rdata = Users::where('username', $request->post('username'))->get();
        if(count($rdata) > 1 || count($rdata) == 0){
            return response()->json(['data'=>'data lebih dari satu']);
        }else{
            $rdata = $rdata->first();
            if($rdata->online == 'TRUE'){
                return response()->json(['msg'=>'anda sudah online di IP ' . $request->ip()]);
            }else{
                if(Hash::check($request->post('password'), $rdata->password)){
                    Users::where('id', $rdata->id)->update(['online'=>'TRUE', 'ip'=>$request->ip()]);
                    $rdata->online = 'TRUE';
                    $rdata->ip = $request->ip();
                    $rdata->user_agent = $request->header('User-Agent');
                    $token = base64_encode($rdata);

                    foreach (json_decode($rdata) as $key => $val){
                        $this->conn->hset($token, $key, $val);
                    }

                    return response()
                        ->json(['data'=>$rdata, 'token'=>$token])
                        ->header('X-csrf-Token', $request->session()->token());
                }else{
                    return response()->json(['data'=>'password tidak diketahui']);
                }
            }
        }
    }

    public function refresh_token(Request $request){
        return response()->json(['msg'=>'token form tercipta'])->header('X-Csrf-Token', $request->session()->token());
    }

    public function logout(Request $request){
        $token = $request->bearerToken();
        $id = $this->conn->hget($token, 'id');
        Users::where('id', $id)->update(['online'=>'FALSE', 'ip'=>$request->ip()]);
        $this->conn->del($token);
        return response()->json(['code'=>200, 'msg'=>'Logout success']);
    }
}
