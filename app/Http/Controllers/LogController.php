<?php

namespace App\Http\Controllers;

use App\Log;

class LogController extends Controller
{
    public function view(){
        $log = Log::all();
        return response()->json(['log'=>$log]);
    }
}
