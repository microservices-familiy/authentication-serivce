<?php

namespace App\Http\Middleware;

use App\Log;
use App\Repositories\PredisRepositori;
use Closure;

class AuthMiddleware
{
    protected $conn;

    public function __construct(PredisRepositori $conn){
        $this->conn = $conn->predis();
    }

    public function handle($request, Closure $next)
    {
        if($request->bearerToken()){
            if($this->conn->hgetall($request->bearerToken()) == null){
                return response()->json(['msg'=>'unauthorized']);
            }else{
                $response = $next($request);

                $log = new Log();
                $log->user_id = $id = $this->conn->hget($request->bearerToken(), 'id');;
                $log->method = $request->method();
                $log->url = $request->fullUrl();
                $log->user_agent = $request->userAgent();
                $log->ip = $request->getClientIp();
                $log->save();

                return $response;
            }
        }else{
            return response()->json(['msg'=>'unauthorized']);
        }
    }
}
