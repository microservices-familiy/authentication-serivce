<?php

namespace App\Http\Middleware;

use Closure;

class CsrfFormMiddleware
{
    public function handle($request, Closure $next)
    {
        if($request->header('X-Csrf-Token')){
            $token = explode(" ", $request->header('X-Csrf-Token'));
            if(count($token)==2){
                if($request->session()->token() == $token[1]){
                    $response = $next($request);
                    $request->session()->regenerate(true);
                    $response->header('X-Csrf-Token', $request->session()->token());
                    return $response;
                }else{
                    return response()->json(['msg'=>"token form expired, silahkan request token"]);
                }
            }else{
                return response()->json(['msg'=>"penulisan token form anda tidak kami ketahui"]);
            }
        }else{
            return response()->json(['msg'=>"Token form wajib diisi"]);
        }
    }
}
