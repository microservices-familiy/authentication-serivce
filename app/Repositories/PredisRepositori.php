<?php

namespace App\Repositories;

use Predis\Client;

class PredisRepositori
{
    protected $conn;

    public function __construct()
    {
        $this->conn = new Client([
            'scheme' => 'tcp',
            'host'   => env('REDIS_HOST'),
            'port'   => env('REDIS_PORT'),
            'password'   => env('REDIS_PASSWORD', null),
        ]);
    }

    public function predis(){
        return $this->conn;
    }
}
