<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Users extends Model
{
    protected $hidden = ['password'];

    protected $appends = ['url_photo'];

    public function getUrlPhotoAttribute(){
        return Storage::url($this->pict_profile);
    }
}
