<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//$router->get('/set', 'TestController@satu');
//$router->get('/del', [
//    'middleware'=>'csrf',
//    'uses'=>'TestController@tiga'
//]);
//$router->get('/get', [
//    'middleware'=>'csrf',
//    'uses'=>'TestController@dua'
//]);

$router->post('/login', 'LoginController@login');

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('/log', 'LogController@view');
    $router->get('/logout', 'LoginController@logout');
    $router->get('/token-form', 'LoginController@refresh_token');
    $router->get('/users', 'UsersController@index');
    $router->get('/user/{id}', 'UsersController@show');
    $router->get('/user/delete/{id}', 'UsersController@destroy');
    $router->group(['middleware'=>'csrf'], function () use ($router){
        $router->post('/user/store', 'UsersController@store');
        $router->post('/user/update/{id}', 'UsersController@update');
    });
});
