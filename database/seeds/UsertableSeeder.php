<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsertableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nama_lengkap'=>"Bayu saputra",
            'email'=>'baysptr@outlook.co.id',
            'no_telp'=>'085707868600',
            'username'=>'baysptr',
            'password'=>Hash::make('password'),
        ]);
    }
}
